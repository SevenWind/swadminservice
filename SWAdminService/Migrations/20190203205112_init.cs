﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace SWAdminService.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CalendarSetting",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    checkOldCellGoldCost = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CalendarSetting", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Rarity",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rarity", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "CalendarRewards",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    needCheckDaysCount = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    code = table.Column<string>(nullable: true),
                    calendarSetting_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CalendarRewards", x => x.id);
                    table.ForeignKey(
                        name: "FK_CalendarRewards_CalendarSetting_calendarSetting_id",
                        column: x => x.calendarSetting_id,
                        principalTable: "CalendarSetting",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Items",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    code = table.Column<string>(nullable: true),
                    image = table.Column<string>(nullable: true),
                    rarity_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items", x => x.id);
                    table.ForeignKey(
                        name: "FK_Items_Rarity_rarity_id",
                        column: x => x.rarity_id,
                        principalTable: "Rarity",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CalendarRewardItems",
                columns: table => new
                {
                    calendarReward_id = table.Column<int>(nullable: false),
                    item_id = table.Column<Guid>(nullable: false),
                    count = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CalendarRewardItems", x => new { x.calendarReward_id, x.item_id });
                    table.ForeignKey(
                        name: "FK_CalendarRewardItems_CalendarRewards_calendarReward_id",
                        column: x => x.calendarReward_id,
                        principalTable: "CalendarRewards",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CalendarRewardItems_Items_item_id",
                        column: x => x.item_id,
                        principalTable: "Items",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "CalendarSetting",
                columns: new[] { "id", "checkOldCellGoldCost" },
                values: new object[] { 1, 100 });

            migrationBuilder.InsertData(
                table: "Rarity",
                columns: new[] { "id", "code" },
                values: new object[,]
                {
                    { 1, "None" },
                    { 2, "COMMON" },
                    { 3, "UNCOMMON" },
                    { 4, "RARE" }
                });

            migrationBuilder.InsertData(
                table: "CalendarRewards",
                columns: new[] { "id", "calendarSetting_id", "code", "name", "needCheckDaysCount" },
                values: new object[,]
                {
                    { 1, 1, "DAILY", "Ежедневная награда", 1 },
                    { 2, 1, "7_DAYS", "7 Дней", 7 },
                    { 3, 1, "14_DAYS", "14 Дней", 14 },
                    { 4, 1, "21_DAYS", "21 День", 21 },
                    { 5, 1, "FULL_MONTH", "Полный месяц", -1 }
                });

            migrationBuilder.InsertData(
                table: "Items",
                columns: new[] { "id", "code", "image", "name", "rarity_id" },
                values: new object[,]
                {
                    { new Guid("55ffb90f-5c4f-48b8-825d-0b96e0a96b1f"), "ScrollOfHiring", "items/scroll.png", "Свиток Найма", 1 },
                    { new Guid("c59df240-37dc-47c0-a2c0-dcaf2403d738"), "MagicAmulet", "items/amulet.png", "Магический амулет", 1 },
                    { new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade"), "Coins", "items/coin.png", "Монеты", 2 },
                    { new Guid("89da2a28-a53d-4484-a1aa-8cf78552fbe2"), "Gold", "items/gold.png", "Золото", 4 }
                });

            migrationBuilder.InsertData(
                table: "CalendarRewardItems",
                columns: new[] { "calendarReward_id", "item_id", "count" },
                values: new object[,]
                {
                    { 1, new Guid("55ffb90f-5c4f-48b8-825d-0b96e0a96b1f"), 1 },
                    { 2, new Guid("55ffb90f-5c4f-48b8-825d-0b96e0a96b1f"), 2 },
                    { 3, new Guid("55ffb90f-5c4f-48b8-825d-0b96e0a96b1f"), 2 },
                    { 4, new Guid("55ffb90f-5c4f-48b8-825d-0b96e0a96b1f"), 5 },
                    { 5, new Guid("55ffb90f-5c4f-48b8-825d-0b96e0a96b1f"), 10 },
                    { 3, new Guid("c59df240-37dc-47c0-a2c0-dcaf2403d738"), 2 },
                    { 4, new Guid("c59df240-37dc-47c0-a2c0-dcaf2403d738"), 5 },
                    { 5, new Guid("c59df240-37dc-47c0-a2c0-dcaf2403d738"), 10 },
                    { 1, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade"), 20000 },
                    { 2, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade"), 600000 },
                    { 3, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade"), 100000 },
                    { 4, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade"), 150000 },
                    { 5, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade"), 500000 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CalendarRewardItems_item_id",
                table: "CalendarRewardItems",
                column: "item_id");

            migrationBuilder.CreateIndex(
                name: "IX_CalendarRewards_calendarSetting_id",
                table: "CalendarRewards",
                column: "calendarSetting_id");

            migrationBuilder.CreateIndex(
                name: "IX_Items_rarity_id",
                table: "Items",
                column: "rarity_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CalendarRewardItems");

            migrationBuilder.DropTable(
                name: "CalendarRewards");

            migrationBuilder.DropTable(
                name: "Items");

            migrationBuilder.DropTable(
                name: "CalendarSetting");

            migrationBuilder.DropTable(
                name: "Rarity");
        }
    }
}
