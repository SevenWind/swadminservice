﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SWAdminService.Migrations
{
    public partial class adddelflagtoitem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "Items",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "CalendarRewards",
                keyColumn: "id",
                keyValue: 5,
                column: "needCheckDaysCount",
                value: 30);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "Items");

            migrationBuilder.UpdateData(
                table: "CalendarRewards",
                keyColumn: "id",
                keyValue: 5,
                column: "needCheckDaysCount",
                value: -1);
        }
    }
}
