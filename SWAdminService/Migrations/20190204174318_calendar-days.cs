﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace SWAdminService.Migrations
{
    public partial class calendardays : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CalendarDays",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    count = table.Column<int>(nullable: false),
                    item_id = table.Column<Guid>(nullable: false),
                    calendarSetting_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CalendarDays", x => x.id);
                    table.ForeignKey(
                        name: "FK_CalendarDays_CalendarSetting_calendarSetting_id",
                        column: x => x.calendarSetting_id,
                        principalTable: "CalendarSetting",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CalendarDays_Items_item_id",
                        column: x => x.item_id,
                        principalTable: "Items",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "CalendarDays",
                columns: new[] { "id", "calendarSetting_id", "count", "item_id" },
                values: new object[,]
                {
                    { 1, 1, 10000, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade") },
                    { 29, 1, 10000, new Guid("55ffb90f-5c4f-48b8-825d-0b96e0a96b1f") },
                    { 28, 1, 1, new Guid("c59df240-37dc-47c0-a2c0-dcaf2403d738") },
                    { 27, 1, 1, new Guid("55ffb90f-5c4f-48b8-825d-0b96e0a96b1f") },
                    { 26, 1, 10000, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade") },
                    { 25, 1, 10000, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade") },
                    { 24, 1, 10000, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade") },
                    { 23, 1, 10000, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade") },
                    { 22, 1, 10000, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade") },
                    { 21, 1, 1, new Guid("c59df240-37dc-47c0-a2c0-dcaf2403d738") },
                    { 20, 1, 1, new Guid("55ffb90f-5c4f-48b8-825d-0b96e0a96b1f") },
                    { 19, 1, 10000, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade") },
                    { 18, 1, 10000, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade") },
                    { 17, 1, 10000, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade") },
                    { 30, 1, 1, new Guid("55ffb90f-5c4f-48b8-825d-0b96e0a96b1f") },
                    { 16, 1, 10000, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade") },
                    { 14, 1, 1, new Guid("c59df240-37dc-47c0-a2c0-dcaf2403d738") },
                    { 13, 1, 1, new Guid("55ffb90f-5c4f-48b8-825d-0b96e0a96b1f") },
                    { 12, 1, 10000, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade") },
                    { 11, 1, 10000, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade") },
                    { 10, 1, 10000, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade") },
                    { 9, 1, 10000, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade") },
                    { 8, 1, 10000, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade") },
                    { 7, 1, 1, new Guid("c59df240-37dc-47c0-a2c0-dcaf2403d738") },
                    { 6, 1, 1, new Guid("55ffb90f-5c4f-48b8-825d-0b96e0a96b1f") },
                    { 5, 1, 10000, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade") },
                    { 4, 1, 10000, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade") },
                    { 3, 1, 10000, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade") },
                    { 2, 1, 10000, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade") },
                    { 15, 1, 10000, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade") },
                    { 31, 1, 1, new Guid("55ffb90f-5c4f-48b8-825d-0b96e0a96b1f") }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CalendarDays_calendarSetting_id",
                table: "CalendarDays",
                column: "calendarSetting_id");

            migrationBuilder.CreateIndex(
                name: "IX_CalendarDays_item_id",
                table: "CalendarDays",
                column: "item_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CalendarDays");
        }
    }
}
