﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SWAdminService.Models {
    public class CalendarSetting {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public int checkOldCellGoldCost { get; set; }

        public List<CalendarReward> rewards { get; set; }

        public List<CalendarDay> days { get; set; }
    }
}
