﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SWAdminService.Models {
    public class CalendarReward {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public int needCheckDaysCount { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public List<CalendarRewardItem> items { get; set; }

        public int calendarSetting_id { get; set; }
        [ForeignKey("calendarSetting_id")]
        public CalendarSetting calendarSetting { get; set; }
    }
}
