﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SWAdminService.Models {
    public class CalendarDay {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public int count { get; set; }

        public Guid item_id { get; set; }
        [ForeignKey("item_id")]
        public Item item { get; set; }

        public int calendarSetting_id { get; set; }
        [ForeignKey("calendarSetting_id")]
        public CalendarSetting calendarSetting { get; set; }
    }
}
