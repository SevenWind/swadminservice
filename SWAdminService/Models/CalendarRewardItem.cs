﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SWAdminService.Models {
    public class CalendarRewardItem {
        [JsonIgnore]
        public int calendarReward_id { get; set; }
        [ForeignKey("calendarReward_id")]
        public CalendarReward calendarReward { get; set; }
        [JsonIgnore]
        public Guid item_id { get; set; }
        [ForeignKey("item_id")]
        public Item item { get; set; }

        public int count { get; set; }
    }
}
