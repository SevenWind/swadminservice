﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SWAdminService.Models;
using static SWAdminService.Models.Enums;

namespace SWAdminService.Controllers {
    [Route ("api/v1/[controller]/[action]")]
    [ApiController]
    [Authorize (Roles = AuthRole.ADMIN, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AdminController : ControllerBase {

        private readonly SWAdminDbContext db;
        private readonly ILogger<AdminController> logger;

        public AdminController (SWAdminDbContext _db, ILogger<AdminController> _loggerFactory) {
            db = _db;
            logger = _loggerFactory;
        }

        [HttpPost ("day={day}")]
        public async Task<IActionResult> SaveCalendarCellReward (int day, CalendarDay reward) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var dbDay = await db.CalendarDays.FindAsync (day);
                    if (dbDay == null) {
                        return NotFound ("Выбран некорректный день");
                    }
                    var item = await db.Items.FindAsync (reward.item_id);
                    if (item == null) {
                        return NotFound ("Предмет не найден в базе!");
                    }
                    dbDay.item_id = reward.item_id;
                    dbDay.count = reward.count;
                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok (true);
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        [HttpPost ("rewardId={rewardId}&itemId={itemId}")]
        public async Task<IActionResult> DeleteRewardItem (int rewardId, Guid itemId) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var dbReward = await db.CalendarRewards.FindAsync (rewardId);
                    if (dbReward == null) {
                        return NotFound ("Награды с таким ID не существует");
                    }
                    var item = await db.CalendarRewardItems.FirstOrDefaultAsync (a => a.calendarReward_id == rewardId && a.item_id == itemId);
                    if (item == null) {
                        return NotFound ();
                    }
                    db.CalendarRewardItems.Remove (item);
                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok (true);
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        [HttpPost ("rewardId={rewardId}&itemId={itemId}&count={count}")]
        public async Task<IActionResult> AddRewardItem (int rewardId, Guid itemId, int count) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var dbReward = await db.CalendarRewards.Include (a => a.items).FirstOrDefaultAsync (a => a.id == rewardId);
                    if (dbReward == null) {
                        return NotFound ("Награды с таким ID не существует");
                    }
                    if (dbReward.items.Count >= 6) {
                        return BadRequest ("В награде уже максимальное количество предметов");
                    }
                    var item = await db.Items.FindAsync (itemId);
                    if (item == null) {
                        return NotFound ("Предмет не найден в базе!");
                    }
                    if (item.isDeleted) {
                        return BadRequest ("Предмет уже удален. Выберите другой!");
                    }
                    var existingRewardItem = await db.CalendarRewardItems.FirstOrDefaultAsync (a => a.calendarReward_id == dbReward.id && a.item_id == item.id);
                    if (existingRewardItem == null) {
                        await db.CalendarRewardItems.AddAsync (new CalendarRewardItem () {
                            calendarReward_id = dbReward.id,
                                item_id = item.id,
                                count = count
                        });
                    } else {
                        existingRewardItem.count = count;
                    }

                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok (true);
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        [HttpPost ("cost={cost}")]
        public async Task<IActionResult> SaveCheckOldCellGoldCost (int cost) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var setting = await db.CalendarSetting.FirstAsync ();
                    if (setting == null) {
                        return NotFound ();
                    }
                    setting.checkOldCellGoldCost = cost;
                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok (true);
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        [HttpPost ("{itemId}")]
        public async Task<IActionResult> DeleteItem (Guid itemId) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var item = await db.Items.FindAsync (itemId);
                    if (item == null) {
                        return NotFound ();
                    }
                    item.isDeleted = true;
                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok (true);
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        [HttpPost ("{itemId}")]
        public async Task<IActionResult> RestoreItem (Guid itemId) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var item = await db.Items.FindAsync (itemId);
                    if (item == null) {
                        return NotFound ();
                    }
                    item.isDeleted = false;
                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok (true);
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        [HttpPost]
        public async Task<IActionResult> SaveItem ([FromBody] Item item) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var dbItem = await db.Items.FirstOrDefaultAsync (a => a.id == item.id);
                    if (dbItem == null) {
                        // create new
                        dbItem = new Item ();
                        dbItem.id = item.id;
                        dbItem.name = item.name;
                        dbItem.code = item.code;
                        dbItem.image = item.image;
                        dbItem.rarity_id = item.rarity_id;
                        dbItem.characterId = item.characterId;
                        await db.Items.AddAsync (dbItem);
                        await db.SaveChangesAsync ();
                        tr.Commit ();
                        return Ok ();
                    }
                    // update
                    dbItem.name = item.name;
                    dbItem.code = item.code;
                    if (!string.IsNullOrWhiteSpace (item.image) && item.image != dbItem.image)
                        dbItem.image = item.image;
                    dbItem.rarity_id = item.rarity_id;
                    dbItem.characterId = item.characterId;

                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok ();
                }
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }
    }
}