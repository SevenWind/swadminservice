﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SWAdminService.Models;
using SWAdminService.Utility;
using static SWAdminService.Models.Enums;

namespace SWAdminService.Controllers {
    [Route ("api/v1/[controller]/[action]")]
    [ApiController]
    public class CalendarController : ControllerBase {

        private readonly SWAdminDbContext db;
        private readonly ILogger<CalendarController> logger;

        public CalendarController (SWAdminDbContext _db, ILogger<CalendarController> _loggerFactory) {
            db = _db;
            logger = _loggerFactory;
        }

        [ProducesResponseType (201, Type = typeof (string))]
        [ProducesResponseType (500)]
        [Authorize (Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet]
        public IActionResult GetCalendarInfo () {
            try {
                var calendarSetting = db.CalendarSetting.Include (a => a.rewards).Include (a => a.days).First ();

                //Подгрузка необходимых зависимостей
                var rewardsCount = calendarSetting.rewards.Count;
                for (int i = 0; i < rewardsCount; i++) {
                    db.Entry (calendarSetting.rewards[i]).Collection (a => a.items).Load ();
                    var items = calendarSetting.rewards[i].items;
                    var itemsCount = items.Count;
                    for (int j = 0; j < itemsCount; j++) {
                        db.Entry (items[j]).Reference (a => a.item).Load ();
                        db.Entry (items[j].item).Reference (a => a.rarity).Load ();
                    }
                }
                var daysCount = calendarSetting.days.Count;
                for (int i = 0; i < daysCount; i++) {
                    db.Entry (calendarSetting.days[i]).Reference (a => a.item).Load ();
                    db.Entry (calendarSetting.days[i].item).Reference (a => a.rarity).Load ();
                }

                calendarSetting.days = calendarSetting.days.OrderBy (a => a.id).ToList ();

                return Ok (JsonHelpers.SerializeObjectWithLoopIgnore (calendarSetting));
            } catch (Exception e) {
                logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }
    }
}